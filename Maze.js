const maze = (maxX, maxY) => {
    for (var x = 0; x < maxX; x++) {
        let sign = ''
        for (var y = 0; y < maxY; y++) {
            if (x == 0 && y == 1) {
                sign += ' ';
            } 
            else if (x === maxX-1 && y == (maxY - 2)){
                sign += ' ';
            }
            else {
                if(x%2 !== 0 && x!== 0 && x !== maxX-1){
                    if(y === 0 || y === maxY-1){
                        sign += '@';
                    } else {
                        sign += ' ';
                    }
                }
                else if(x%2 == 0 && x!== 0 && x !== maxX-1){
                    if(y === maxY-2){
                        sign += ' '
                    } else {
                        sign += '@';
                    }
                }
                else {
                    sign += '@';
                }
            }

        }
        console.log(sign)
    }
    return ''
}
console.log(maze(15, 15))